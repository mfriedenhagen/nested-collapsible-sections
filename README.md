# Nested Collapsible Sections

Nested sections should be collapsed as completely

* Take a look at the console log of job "test".
* I would expect that "In outer section 2" is only shown 
  when I expand "SECTION: Outer"
* This is only a UI problem, runtime information for 
  sections is collected correctly.

## License

This code is licensed under the Apache 2.0 license

